import {Component, OnInit} from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { Tvshow } from '../models/data';

@Component({
    selector:'date',
    templateUrl:'date.page.html',
    styleUrls: ['date.page.scss']

})

export class DatePage implements OnInit{

    
    constructor(private activatedRoute:ActivatedRoute, private dataService: DataService){}

    ngOnInit(){
        this.activatedRoute.queryParams.subscribe(params =>{
           const id = params['id'];
           if(id){
               this.dataService.getTvshowsById(+id).then(res =>{
                   console.log('Result:',res);
               })
           }
            console.log(params['id']);
        });
    }
    
    //private getUniqueDates(tvshows: Tvshow[]):any[]{

    //}
}