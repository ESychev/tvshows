import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import{Channel} from '../models/data'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public channels: Channel[]=[];

  constructor(private dataService: DataService){

  }
  ngOnInit(){
    this.dataService.getChanels().then(res =>{
      console.log('load finished');
      this.channels=res;
    })
  }
}
