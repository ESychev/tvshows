export interface Channel{
    channel_id: number;
    name:string;
    logo: string;
    genres: number[];
}

export interface  Tvshow{
    channel_id: number;
    date: string;
    start: number;
    stop:  number;
    title: string;
    ts: number;
    tvshow_id: string;
    
}