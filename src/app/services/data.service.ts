import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import  {map} from 'rxjs/operators';
import { Channel, Tvshow } from '../models/data';



@Injectable()
export class DataService{
    private readonly BASE_URL = 'http://api.persik.by/v2/';
    constructor( private http:HttpClient){}

    getChanels():Promise<Channel[]>{
        return this.http.get<ServerChannels>(this.BASE_URL.concat('content/channels')).pipe(map(res=>res.channels)).toPromise();
    }
// безопасный запрос 
    getTvshowsById(channel_id: number):Promise<Tvshow>{
        const params: HttpParams=new HttpParams()
        .set('channels[]',channel_id.toString());
       return this.http.get<any>(this.BASE_URL.concat('epg/tvshows'),{params})
       .pipe(map(res=>res.tvshows.items)).toPromise();
    }
}

interface ServerChannels{
    channels: Channel[];
}

interface ServerTvshow{
    tvshows:{
        items: Tvshow[];
        total: number;
    }
}